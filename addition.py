
'''
@method addition:
@param firstNumber:
@param secondNumber:
@return:

'''
# The below function takes two integer and return its sum

def addition(firstNumber,secondNumber):
	return firstNumber + secondNumber

sum = addition(3,4)
print(sum)

'''
@method multiply:
@param firstNumber:
@param secondNumber:
@return:

'''

# The below function takes two integer and return its product

def multiply(firstNumber,secondNumber):
	return firstNumber * secondNumber

product = multiply(4,3)
print(product)